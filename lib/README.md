该目录文件为讯飞官方语音听写（流式版）demo 中封装的音频采集类、接口方案。并做了修改。
index.vue  中的  "socketUrl"  未用户在 科大讯飞网站获取 
const API_SECRET = 'xxx'
const API_KEY = 'xxxx'

 
并且自行计算 获取；官方demo 中有详细说明：
官方 demo 链接：https://www.xfyun.cn/doc/asr/voicedictation/API.html#%E6%8E%A5%E5%8F%A3%E8%AF%B4%E6%98%8E


function getWebSocketUrl() {
  return new Promise((resolve, reject) => {
    // 请求地址根据语种不同变化
    var url = 'wss://iat-api.xfyun.cn/v2/iat'
    var host = 'iat-api.xfyun.cn'
    var apiKey = API_KEY
    var apiSecret = API_SECRET
    var date = new Date().toGMTString()
    var algorithm = 'hmac-sha256'
    var headers = 'host date request-line'
    var signatureOrigin = `host: ${host}\ndate: ${date}\nGET /v2/iat HTTP/1.1`
    var signatureSha = CryptoJS.HmacSHA256(signatureOrigin, apiSecret)
    var signature = CryptoJS.enc.Base64.stringify(signatureSha)
    var authorizationOrigin = `api_key="${apiKey}", algorithm="${algorithm}", headers="${headers}", signature="${signature}"`
    var authorization = btoa(authorizationOrigin)
    url = `${url}?authorization=${authorization}&date=${date}&host=${host}`
    //url = `${url}?authorization=${authorization}&host=${host}`
    //url = 'wss://iat-api.xfyun.cn/v2/iat?authorization=YXBpX2tleT0iM2JmMmIwOTYwODJhODEyNzQ1MjQ1Nzg5NTBlODU5YzIiLCBhbGdvcml0aG09ImhtYWMtc2hhMjU2IiwgaGVhZGVycz0iaG9zdCBkYXRlIHJlcXVlc3QtbGluZSIsIHNpZ25hdHVyZT0ibTd2bWJUODZNdzZvSnhuaUxVQlE3UU9ueXdRK1NobG4veXVUc0lNaHNQYz0i&date=Mon, 31 May 2021 09:28:56 GMT&host=iat-api.xfyun.cn'
    console.log("url:",url)

    resolve(url)
  })
}