const environment = {
	// 开发环境配置
	development: {
		// 本地部署的后端
		 baseURL: 'http://127.0.0.1:8080',
		
		// 直接使用线上后端
//		baseURL: 'http://vue.ruoyi.vip/prod-api'
	},
	// 生产环境配置
	production: {
		 baseURL: 'http://117.50.184.182:8080',
	}
}

module.exports = {
  environment: environment[process.env.NODE_ENV]
}